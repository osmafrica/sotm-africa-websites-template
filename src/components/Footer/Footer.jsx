import FooterWidget from './FooterWidget';
import './style.css';
import { ReactComponent as Logo } from 'assets/img/logo.svg';
import { ReactComponent as HostLogo } from 'assets/img/cameroon-flag.svg';
import { siteConfiguration } from 'siteConfig';
import { useLanguageContext } from 'context/LanguageProvider';

const Footer = () => {
	const { footerLogoURL, footer } = siteConfiguration;
	const { currentLanguage } = useLanguageContext();
	return (
		<footer>
			<div className="footer-container wrapper">
				<div className="footer-logo-container">
					<a href={footerLogoURL}>
						<Logo className="footer-logo" />
					</a>
				</div>
				<div className="wrapper-links">
					{footer
						.filter((footerItem) => footerItem.isActive)
						.map((footerItem, id) => (
							<FooterWidget
								key={id}
								widgetTitle={footerItem.title[currentLanguage]}
								WidgetList={footerItem.links}
							/>
						))}
				</div>
			</div>
			<div className="copyright-wrapper">
				<p>
					<span>Built with</span>
					<a href="/">
						<HostLogo />
					</a>
				</p>
				<p className="copyright">
					{siteConfiguration.copyright[currentLanguage]}
				</p>
			</div>
		</footer>
	);
};

export default Footer;
