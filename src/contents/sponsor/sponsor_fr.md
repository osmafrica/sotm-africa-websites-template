# L'Open Mapping comme outil d'appui au développement local en Afrique

**Download Prospectus**:

- [Français](/sotmafrica2023_fr.pdf)
- [English](/sotmafrica2023_en.pdf)

State of the Map Africa est une conférence biannuelle des communautés OpenStreetMap à travers l'Afrique. La conférence rassemble un groupe diversifié de contributeurs OSM, y compris des cartographes, des utilisateurs, des développeurs et, plus généralement, des professionnels des SIG, pour apprendre et partager sur OSM. La conférence a été une bonne plateforme pour partager les connaissances, favoriser les relations et encourager de nouvelles collaborations.

Le succès de la conférence dépend entièrement du soutien financier d'organisations comme la vôtre en nous permettant de faire venir et d'accueillir plus de 40 intervenants et environ 200 participants de la communauté OpenStreetMap Africa représentant plus de 30 pays africains.

Votre soutien nous aidera à accroître la diversité au sein de la communauté OSM et à améliorer la contribution et l'utilisation des OSM sur le continent. En tant que nos sponsors, vous pourrez :

- Vous connecter à la communauté OSMAfrica et établir des collaborations avec des individus talentueux du continent.
- Développer la notoriété de votre marque et accroître la visibilité de vos produits et services au sein de la communauté OSM.
- Apprendre et partager sur les développements actuels de OSM au niveau régional et international.
- Soutenir notre engagement à accroître l'utilisation de données ouvertes, en particulier OSM, pour résoudre les problèmes du monde réel auxquels nous sommes confrontés au quotidien.

Vous souhaitez parrainer SotM Africa 2023 ? Consultez nos offres de parrainage et n'hésitez pas à nous contacter. Nous serons heureux de répondre à toutes vos questions et nous travaillerons avec vous pour trouver la formule de parrainage idéale.

Vous ne voyez pas ce que vous recherchez ? Prenez contact avec nous via **sponsor** [at] **stateofthemap** [dot] **africa**
