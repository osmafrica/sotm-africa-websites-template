#### Cameroun

Le Cameroun est décrit comme «l'Afrique en miniature», faisant référence au fait que le pays offre la plupart des diversités de l'Afrique, de la culture (avec le français et l'anglais comme ses deux langues officielles) à ses paysages variés qui représentent les principales zones climatiques du continent. Les plus de trois cents (300) sites touristiques au Cameroun et ses 258 groupes ethniques offrent autant de traditions que de diversités culturelles.

#### Yaoundé

Surnommée «la ville aux sept collines», est une ville au relief montagneux, la capitale politique du Cameroun. Yaoundé est le siège du gouvernement politique du Cameroun qui offre une grande flexibilité dans l'interaction entre les différentes institutions (autorisations, invitations, partenariats, etc.) car la plupart de leurs bureaux y sont situés. Elle dispose d'infrastructures de qualité (salles de conférence, hôtels, centres spécialisés, sites touristiques, etc.) et de dispositions adéquates (sécurité des lieux, ambassades) pour accueillir et assurer un séjour harmonieux à quiconque venant d'autres pays.
La ville a lancé la cérémonie d'ouverture de la Coupe d'Afrique des Nations Total CAN 2021 et a offert une satisfaction totale au public venu pour l'événement.

Le Cameroun a réussi à organiser le SOTM Cameroun en 2017 et a participé activement à l'organisation de SOTM Africa 2019 à Grand Bassam. Il est également l'hôte de SOTM Africa 2023.

#### Informations Visa

Les ressortissants exemptés de visa: République centrafricaine, Tchad, Congo, Guinée équatoriale, Gabon, Nigeria. Veuillez vérifier ici si votre pays est exempté: [https://www.passportindex.org/](https://www.passportindex.org/).

**Passeports spéciaux:** les titulaires de passeports de catégorie diplomatique ou de service délivrés aux ressortissants de la Chine, de l'Italie et de la Turquie n'ont pas besoin de visa. Les titulaires de passeports diplomatiques et de service peuvent obtenir un visa à l'arrivée s'ils ont un ordre de mission et un billet de retour.
**Nationalités soumises à un visa:** les citoyens de toutes les autres nationalités peuvent obtenir leur visa dans les ambassades.

Nous tenons à souligner que les visas ne seront délivrés que par les représentations diplomatiques et consulaires du Cameroun. Aucun visa ne peut être délivré à l'arrivée au Cameroun. Les participants ressortissants de pays nécessitant un visa doivent soumettre un formulaire de demande rempli, une photo de passeport et un document de voyage ordinaire valable d'au moins six mois. Les participants doivent également présenter une lettre d'invitation de l'organisation hôte et d'autres documents si nécessaire.
Pour demander un VISA, vous devrez soumettre une lettre d'invitation à l'ambassade.
Vous pouvez recevoir une lettre d'invitation des organisateurs de la conférence qui confirmera que vous êtes un participant à la conférence. Pour obtenir une lettre d'invitation, vous devez envoyer une copie de votre passeport par e-mail. Sur la copie, votre nom complet, date de naissance, date d'expiration et le numéro de passeport doivent être clairement visibles.
Vous pouvez recevoir une lettre d'invitation une fois que vous avez réussi votre inscription et que vous avez payé vos frais d'inscription. Gardez à l'esprit que vous devez demander un VISA au plus tard 1 mois avant la conférence, car cela prend du temps pour obtenir le VISA.

**Remarque:** un passager qui n'a pas de papiers d'identité camerounais (passeport ou carte d'identité) est considéré comme un passager étranger même s'il a une multi-nationalité et doit donc être en possession d'un visa.
En savoir plus: _Politique de visa pour le Cameroun_.

#### Voyage / Transport:

Yaoundé occupe une place centrale au Cameroun car toutes les grandes routes mènent à la capitale. Les infrastructures ont été particulièrement renouvelées récemment car la CAN a eu lieu au Cameroun en 2022.
Le Cameroun dispose de 4 aéroports internationaux à Douala, Yaoundé, Garoua et Maroua, les plus grands étant ceux de Douala et Yaoundé. Ils desservent notamment Camair-Co, Air France, Swissair, Turkish Airlines, Royal Air Maroc, Brussels Airlines, Air Côte d'Ivoire, Ethiopian Airlines et Kenyan Airways. Le Cameroun dispose également d'un bon système ferroviaire qui permet d'atteindre facilement de nombreuses villes. La société Camrail est opérationnelle.

- Les principaux moyens de transport dans les villes sont les taxis et les taxis-motos connus sous le nom de "Benskin". Les taxis peuvent être loués individuellement ou en groupe pour environ 2 500 à 3 000 FCFA (3,50 $ / 5€) par trajet.
- Il existe également l'application "Yango" pour le transport urbain: [https://yango.com/en_no/passenger/](https://yango.com/en_no/passenger/) .

De l'aéroport à la ville, nous disposons de taxis de qualité à l'aéroport international de Nsimalen. L'application de transport Yango est également disponible et très fiable. En outre, pour assurer le déplacement des participants, nous prévoyons de nous associer à une agence de transport locale ; nous fournirons également un véhicule qui assurera le transport d'urgence.

#### Test Covid

Le 20 juin 2023, le gouvernement du Cameroun a levé toutes les restrictions d'entrée liées au Covid-19. Plus de test à l’arrivée et plus de présentation de justificatif de vaccination.

#### Hébergement

Le comité d'organisation local a négocié des tarifs avantageux avec divers hôtels dans différentes catégories de prix pour vous assurer le meilleur tarif possible. Les tarifs spéciaux ne s'appliquent qu'aux participants de la conférence et ne sont disponibles que lors de la réservation via le formulaire d'inscription à la conférence. Veuillez noter que tous les prix incluent le petit déjeuner, la TVA et toutes les taxes.

Pendant la conférence, quelques membres de la communauté locale proposent un co-hébergement gratuit pour ceux qui n'auront pas les moyens financiers de payer un hôtel. Il y aura 100 places disponibles. Si vous êtes intéressé, n'hésitez pas à nous contacter. Un arrangement est également en cours de négociation avec les différentes ambassades pour fournir un hébergement aux ressortissants de leur pays.

#### Conference Venue

See the conference venue, United Hotel on [Bookings](https://www.booking.com/hotel/cm/united-mbankomo.en-gb.html#map_closed). Check here for accommodation options. [Link](https://drive.google.com/file/d/1FmG_E9yVaSmUkyesaoRHNf5NrrZE-Zvy/view?usp=sharing).

#### Nourritures/Mets

Le Cameroun par sa diversité culturelle dispose d’une très grande variété de mets locaux qui accompagnent les fêtes et cérémonies officielles ou traditionnelles. Faits à base des vivres issus de ses activités agricoles pratiquées par les indigènes ou les professionnelles du domaine.

La liste n'est pas exhaustive et nous citerons entre autres:

- **Pommes de terre pilées:** Ce plat est originaire de la région de l’ouest du pays est fait à base de pommes de terre, du haricot rouge ou noir sec, d’huile de palme et d’autres épices. Particulièrement apprécié, ce plat est un délice à déguster!
- **Le Eru (avec le Water Fufu):** Originaire de la partie anglophone, ce plat est aussi un des plats appréciés au Cameroun. Il est fait à base des feuilles de Eru ou water fufu (légume) , d’écrevisses, du poisson fumé, de la peau de bœuf, du piment, d’huile de palme et des épices.
  Ainsi ce plat s’accompagne du couscous à base de farine de manioc (fufu), de tapioca, du plantain,...etc
- **Le Koki:** Ce plat est un délice fait à base du niébé, d'huile de palme, des feuilles de bananier (servant d’emballage).
  Il se consomme bien chaud accompagné de la banane plantain, du manioc ou des patates douces.
- **Le Ashu ou Taro:** Il se fait à partir du Ashu ou Taro, d’huile de palme et des épices et autres ingrédients. Ce met accompagne de grandes cérémonies traditionnelles au Cameroun chez certains peuples et se déguste à base des mains lavées ou une cuillère pour les moins habitués.
- **Le Ndolé:** Nom Camerounais de variétés alimentaires de Vernonia. Il s’agit d’une plante légumière dont les feuilles sont consommées. Il est particulièrement un des mets les plus appréciés au Cameroun et accompagne toutes les cérémonies. Il est important de rappeler que ce légume à aussi des vertus thérapeutiques.
  Il s’accompagne du riz et des tubercules.
- **La sauce gombo:** À base du gombo frais et d’huile d’arachide, de la viande ou du poisson et d’autres ingrédients, ce met gluant est aussi beaucoup aimé et s’accompagne du couscous maïs ou du couscous manioc.
- **Le Tassba, Mbongo Tchobi, Kondrè, Le Sanga’a**…etc sont aussi d’autres divers mets qu’on trouve au Cameroun.
  …etc

Au Cameroun nous consommons une grande variété de viandes, entre autres: la viande de bœuf, du porc, des chèvres et moutons, des poulets, du lapin et une grande variété de poissons. Alors il est important de noter que l’Afrique est aussi valablement représentée dans nos plats, n’ayez donc crainte de venir chez nous car vous trouverez de quoi mettre sous la dent avec appétit. Nous vous attendons impatiemment!!!
