#### Cameroon

Cameroon is described as "Africa in miniature", with reference to the fact that the country offers most of Africa's diversity from cultural (with French and English as its two official languages) to it's diverse landscapes that represent the continent's major climatic zones. There are over three hundred (300) tourist sites in Cameroon and its 258 ethnic groups offer equally as many traditions as cultural diversities.

#### Yaoundé

Yaounde, nicknamed "City of Seven Hills" is a city of mountainous relief; the political capital of Cameroon. Yaounde is the seat of Cameroon's political government, which offers great flexibility in the interaction between the various institutions (authorizations, invitations, partnerships, etc) because most of their offices are located there. It has quality structures (conference rooms, hotels, hospitals and specialized centers, tourist sites, etc) and adequate provisions (security of the premises, embassies) to welcome and ensure a harmonious stay to anyone coming from other countries.

The city launched the opening ceremony of the Total CAN African cup of Nation 2021 and provided an all-round satisfaction to the public who came for the event.

Cameroon has successfully organized the SOTM Cameroon in 2017, and has actively participated in the organization of SOTM Africa 2019 in Grand Bassam. It is also the host of SOTM Africa 2023.

#### Visa Information

Visa-exempt foreign nationals: Central African Republic, Chad, Congo, Equatorial Guinea, Gabon, Nigeria. Kindly check here to check if your country is exempted: [https://www.passportindex.org/](https://www.passportindex.org/)

**Special passports:** holders of diplomatic or service category passports issued to nationals of China, Italy and Turkey do not require a visa. Holders of diplomatic and service passports can obtain a visa on arrival if they have a mission order and a return ticket.

**Nationalities subject to visa:** citizens of all other nationalities can obtain their visa in embassies.

We would like to advice that visas will only be issued by the Cameroon diplomatic and consular representations. No visas can be issued upon arrival in Cameroon. The participants who are citizens of countries requiring visas should submit a filled application form, passport photo and regular travel document with at least six months validity. The participants should also present a letter-invitation from the host organization and other documents if needed.

To apply for a VISA you will need to submit an Invitation Letter to the Embassy. You can receive Invitation Letter from the Conference Organizers that will confirm that you are a participant in the conference. To obtain an Invitation Letter you have to send a copy of your passport via e-mail. On the copy, your full name, date of birth, date of expiry and the passport number should be clearly visible.

You can receive an Invitation Letter once you have successfully completed your registration and paid your registration fee. Keep in mind that you should apply for a VISA no later than 1 month prior to the conference, as it takes time for VISA issuing.

If you have any additional questions, please contact us.

Note: a passenger who does not have Cameroonian identity papers (passport or identity card) is considered as a foreign passenger even if he / she has a multi-citizenship and must therefore be in possession of a visa.

Lear more: [Visa Policy for Cameroon](https://en.wikipedia.org/wiki/Visa_policy_of_Cameroon)

#### Travel/Transportation

Yaounde has a very central place in Cameroon as all the major roads leads to the capital. Infrastructures have been particularly renewed recently as the AFCON took place in Cameroon in 2022

Cameroon has 4 international airports in Douala, Yaounde, Garoua and Maroua. The biggest been those in Douala and Yaounde. They service Camair-Co, Air France, Swissair, Turkish Airlines, Royal air Maroc, Brussels Airlines, Air Cote-d’ivoire, Ethiopian Airlines and Kenyan Airways
Cameroon has a good rail system which allows people to easily reach many cities. The Camrail company is operational.

- The main means of transportation in the cities are taxis and motorcycle taxis known as “Bendskin”. Taxis can be hired individually or in groups for about 2,500 to 3,000 CFA ($3.50 /5€) per trip.
- There’s also “Yango” Application for Transportation : [https://yango.com/en_no/passenger/](https://yango.com/en_no/passenger/)

From the airport to the city, we have secure city cabs at Nsimalen International Airport. The Yango transport application is also available and very reliable. In addition, to ensure the movement of participants, we plan to partner with a local transportation agency; we will also provide a vehicle that will ensure emergency transportation.

#### Covid Test

On the 20th of June, 2023, the Government of Cameroon has lifted all Covid-19 restrictions for entry. No more test on arrival and no more presenting of proof of vaccination.

#### Accomodation

The local organising committee have negotiated attractive room rates with various hotels in different price categories to ensure you the best possible rate. The special rates only apply to participants of the conference and are available only when booking is made via the registration form when you register for the conference. Please note that all prices include breakfast, VAT and all taxes.

During the conference a few members of the local community offer a free co-rent for those who will not have the financial means to pay a hotel. There will be 100 places available. If you are interested do not hesitate to contact us. An arrangement is also being negotiated with the various embassies to provide Accommodation for nationals of their country.

#### Conference Venue

See the conference venue, United Hotel on [Bookings](https://www.booking.com/hotel/cm/united-mbankomo.en-gb.html#map_closed). Check here for accommodation options. [Link](https://drive.google.com/file/d/1FmG_E9yVaSmUkyesaoRHNf5NrrZE-Zvy/view?usp=sharing).

#### Food

Cameroon’s cultural diversity provides a great variety of local dishes that accompany official or traditional festivals and ceremonies. These dishes are made from crops grown by indigenous farmers or professionals in the field of agriculture.

The list is not exhaustive, and we will mention among others:

- **Mashed Potatoes:** This dish originates from the western region of the country and is made from potatoes, dry red or black beans, palm oil, and other spices. Particularly appreciated, this dish is a delight to taste!

- **Eru (with Water Fufu):** Originating from the Anglophone region, this dish is also one of the most appreciated dishes in Cameroon. It is made from Eru or water fufu leaves (vegetables), crayfish, smoked fish, beef skin, hot pepper, palm oil, and spices. This dish is accompanied by couscous made from cassava flour (fufu), tapioca, plantains, etc.
- **Koki:** This dish is a delight made from cowpeas, palm oil, and banana leaves (used as packaging), it is best enjoyed hot with plantains, cassava, or sweet potatoes.
- **Ashu or Taro:** Made from Ashu or taro, palm oil, spices and other ingredients, this dish accompanies major traditional ceremonies in Cameroon for some ethnic groups and is eaten with freshly washed hands or a spoon for the less accustomed.
- **Ndolé:** A Cameroon name for a variety of Vernonia food. It is a leafy vegetable whose leaves are consumed. This is one of the most appreciated dishes in Cameroon and accompanies all ceremonies. It is important to remember that this vegetable also has therapeutic properties. It is served with rice and tubers.
- **Okra sauce:** Made from fresh okra, peanut oil, meat or fish, and other ingredients, this sticky dish is also very popular and is served with corn couscous or cassava couscous.
- **Tassba, Mbongo Tchobi, Kondrè, Sanga’a,..** etc are other diverse dishes in Cameroon.

In Cameroon, we consume a variety of meats, including beef, pork, goats, sheep, chickens, rabbits, and a variety of fish. It’s important to note that Africa is also adequately represented in our dishes, so do not hesitate to come to Cameroon because you will find plenty to satisfy your appetite. We are eagerly waiting for you!
