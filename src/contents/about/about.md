# Open Mapping as a support tool for Local Development in Africa

The State of the Map Africa (SotM Africa) is a bi-annual regional conference that celebrates the culture of open mapping, open data, GIS and its impact across Africa. The first SotM Africa conference was hosted by the OSM community in Uganda in 2017.
In 2021, It was hosted in Abidjan, and Grand-Bassam in Ivory Coast. The 2021 conference planned in Nairobi, Kenya was virtually hosted (online). 
This year’s conference in Yaounde, Cameroon will continue to build on the strategy envisioned for OpenStreetMap in Africa as a renewed, strong, and growing network, and as part of the global OpenStreetMap and Open GIS movement.

#### OpenStreetMap Africa

OSM Africa is a regional community of contributors, users and supporters of OpenStreetMap from countries within the African continent. This includes mappers, scientific researchers, humanitarians, NGOs, government agencies, small business and global companies having and/or supporting work within the continent.The network is aimed at growing and producing a complete and well detailed map of Africa on OpenStreetMap in order to advance the quality, completeness and sustainability of geospatial data in Africa.
