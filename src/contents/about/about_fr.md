# Open Mapping as a support tool for Local Development in Africa

(SotM Africa) est une conférence régionale biannuelle qui célèbre la culture de la cartographie ouverte, des données ouvertes, des SIG et de leur impact à travers l'Afrique. La première conférence SotM Africa a été organisée par la communauté OSM en Ouganda en 2017. En 2019, elle a été accueillie à Abidjan et à Grand-Bassam en Côte d'Ivoire. En 2021, elle était prévue à Nairobi, Kenya mais a finalement eu lieu en ligne.
La conférence de cette année à Yaounde, Cameroun, continuera à s'appuyer sur la stratégie envisagée pour OpenStreetMap en Afrique, en tant que réseau renouvelé, fort et en pleine croissance, et dans le cadre du mouvement mondial OpenStreetMap et Open GIS.

#### OpenStreetMap Africa

OSM Africa est un réseau de communautés locales OpenStreetMap, des communautés de toute l'Afrique qui se donnent la main pour partager des ressources et collaborer pour se développer et produire une carte complète et bien détaillée de l'Afrique sur OpenStreetMap afin de faire progresser la qualité, l'exhaustivité et la durabilité des données géospatiales en Afrique.
