Du 1er au 3 décembre 2023, la communauté OSM Afrique se réunira pour une nouvelle conférence passionnante : le State of the Map Africa 2023. Cette conférence vise à rassembler les passionnés d'OpenStreetMap d'Afrique et d'ailleurs.

Alors que les plateformes open-source et collaboratives continuent d'évoluer, la conférence de cette année vise à mettre en évidence la façon dont la cartographie ouverte a joué et continue de jouer un rôle puissant dans le développement local en Afrique. La cartographie ouverte permet aux communautés de mieux comprendre et de répondre à leurs propres besoins et défis, qu'il s'agisse d'améliorer les infrastructures et les services ou de répondre à d'autres situations d'urgence, y compris les catastrophes naturelles, entraînant ainsi un changement positif.

La conférence sera l'occasion pour les cartographes de partager et d'échanger des idées, des connaissances et des expériences, d'élargir leur réseau et de dégager des idées pour développer OSM sur le continent. Nous invitons les membres de la communauté OpenStreetMap et les autres utilisateurs de données OSM à partager avec nous leurs projets et idées en matière de cartographie, ainsi que leurs expériences OSM, tant au niveau individuel que communautaire ou institutionnel. Qu'il s'agisse de cartographes débutants, d'utilisateurs de données OSM ou de leaders communautaires, nous aimerions vous entendre !

Le thème de cette année étant **la cartographie ouverte en tant qu'outil de soutien au développement local en Afrique**, nous souhaitons en savoir plus sur les sujets suivants

- La collaboration entre parties prenantes pour promouvoir les outils de cartographie ouverte pour le développement local.
- Comment les données ouvertes ont aidé les gouvernements et les communautés à soutenir des interventions, par exemple des projets liés à la santé, des projets humanitaires, etc.
- Comment la cartographie aide-t-elle l'Afrique à favoriser le développement ?
- Des projets cartographiques qui mettent en évidence la manière dont la cartographie ouverte peut entraîner des changements positifs pour les communautés locales.
- L'avenir de la cartographie : comment OpenStreetMap change la donne ?
- Soutien politique aux outils de cartographie ouverte afin de promouvoir le partage des données et la collaboration.
- Engagement avec les communautés locales pour s'assurer que les outils de cartographie ouverte reflètent leurs besoins et leurs priorités.
- Les collaborations cartographiques entre différentes entités, communautés et pays.

#### Pistes

Nous recherchons des propositions dans ces catégories :

- **Cartographie et visualisation de données**

Cette piste concerne tout ce qui a trait à la cartographie et à la visualisation de données. Nous accueillons toutes les soumissions qui démontrent l'utilisation des données OpenStreetMap et des plateformes, outils et logiciels comme QGIS, GrassGIS, Blender, etc., pour produire et partager des cartes et des visualisations convaincantes.

- **Cartographie** Il existe plusieurs façons de contribuer à OSM et d'éditer la carte ! Dans cette piste, nous aimerions savoir comment vous utilisez les outils existants et nouveaux, les applications mobiles et les plateformes pour la collecte, l'édition et la validation des données. Cela inclut également les expériences de test, les avantages et les inconvénients de chacun, etc.
- **Analyse des données** Cette piste concerne les recherches, les projets et les travaux scientifiques réalisés pour évaluer la qualité des données OpenStreetMap, les modèles de contribution et l'utilisation.
- **Communauté** Cette piste est axée sur les présentations de communautés et de réseaux ouverts dans le domaine de la cartographie ouverte et de l'espace géospatial en général. Qu'il s'agisse de communautés OSM locales et régionales, de chapitres YouthMappers, de chapitres OSGeo ou d'autres réseaux de groupes, nous aimerions entendre parler de vos activités communautaires, de vos plans de durabilité et de vos initiatives visant à améliorer la croissance, la diversité et l'inclusion de la communauté.
- **Volet innovation** Avez-vous construit des solutions web en utilisant OSM ? Nous aimerions connaître votre processus de conception et de développement d'applications web, de portails et de cartes narratives à l'aide de l'API OpenStreetMap, de différentes bibliothèques et de plateformes tierces.
- **Informatique scientifique** Cette piste est dédiée aux personnes et/ou organisations qui souhaitent partager leurs cas d'utilisation de la cartographie avec l'intelligence artificielle, l'apprentissage automatique, la technologie de la réalité (AR/VR/MR), etc.

#### Types de soumission

- **Présentations** Cette session est principalement dédiée au partage de votre travail avec la communauté OSM, qu'il soit lié à la cartographie, à la recherche, au développement d'applications ou à d'autres domaines spécifiques.

  - **Les présentations** : Elles durent généralement 20 minutes et 5 minutes supplémentaires sont prévues pour les questions-réponses.
  - **Présentations éclairs** : Ce sont des exposés rapides de 5 minutes, avec 2 minutes supplémentaires pour les questions-réponses.

- **Atelier** sLes ateliers durent entre une heure et deux heures. Le présentateur animera des sessions pratiques qui démontrent des concepts techniques afin de permettre aux participants de suivre facilement la démonstration. Les ateliers auront lieu avant et pendant la conférence. Ils comprendront des sessions simples et introductives sur la prise en main d'OSM et des sessions plus techniques présentant des tutoriels spécifiques à la cartographie, le développement de paquets géospatiaux, la classification d'images, les applications AI/ML en cartographie.

- **Panel / Table-ronde**
  Les panels et tables rondes dureront une heure. Ces discussions constitueront un forum précieux pour un éventail diversifié de parties prenantes qui pourront partager les meilleures pratiques et explorer les opportunités et les défis de la cartographie ouverte. Les participants peuvent être des dirigeants de communautés, des organisations locales, des praticiens du développement, des fonctionnaires et des universitaires, entre autres.

La discussion pourrait porter sur différents thèmes, tels que :

- L'importance de renforcer les capacités en matière de cartographie ouverte en Afrique. Cela pourrait inclure des programmes de formation et d'éducation, ainsi que des efforts pour promouvoir des approches plus inclusives et participatives de la cartographie.
- Comment développer des modèles de financement plus solides et durables pour les initiatives de cartographie ouverte. Bien que de nombreux projets aient été couronnés de succès, l'obtention d'un financement et d'un soutien à long terme peut s'avérer difficile. Ce groupe pourrait explorer des stratégies pour construire des modèles de financement durables qui peuvent soutenir les efforts de cartographie ouverte sur le long terme.
- Aborder les obstacles liés au genre à la participation à la cartographie ouverte. Les femmes sont confrontées à des défis liés à l'accès à la technologie et à des normes culturelles qui limitent leur capacité à participer aux activités de cartographie. Ce groupe pourrait explorer des stratégies pour lever ces obstacles et promouvoir une participation inclusive, en établissant une confiance et des relations avec les groupes de femmes par le biais d'efforts de sensibilisation et d'engagement.
- Veiller à ce que les besoins et les perspectives des femmes soient reflétés dans les données cartographiques. Les données cartographiques peuvent être plus inclusives et refléter la vie des femmes si des stratégies peuvent être développées pour s'en assurer. Ce groupe pourrait étudier comment la compréhension des besoins des femmes et l'obtention de leurs points de vue peuvent contribuer à améliorer la cartographie, ainsi que les stratégies visant à impliquer les femmes dans la cartographie.

**Pour le panel/table ronde, nous souhaitons que les propositions reflètent une expertise pertinente dans les thèmes sélectionnés ainsi que des perspectives uniques que vous apporterez à la discussion. Les propositions doivent porter sur un seul [1] thème parmi ceux énumérés ci-dessus.**

##### Autres types de sessions

Les autres types de sessions comprennent :

- Sessions "Bird of a feather" (BoFs)
- Présentations de posters
- Sessions de cartographie par drone

#### Détails de la Soumission

- Enregistrement et matériel
  - Cela dépend de la décision prise quant à savoir si nous aurons une conférence physique ou en ligne. Mais idéalement, l'équipe de SotM Africa prévoit d'enregistrer toutes les sessions pendant l'événement.
- Déroulement de la conférence
  - A déterminer
- Inscription des intervenants :
  - Pour participer à la conférence State of the Map Africa 2021, tous les intervenants et les participants devront s'inscrire à la conférence. Plus de détails seront annoncés prochainement.

#### Comment nous contacter

Vous pouvez nous adresser toutes vos communications à l'adresse [program@stateofthemap.africa](mailto:program@stateofthemap.africa)

# Empreinte Académique

Cette année, la conférence State of the Map Africa 2023 présentera la première édition de l’Empreinte Académique - une journée complète de sessions consacrées à la recherche universitaire sur et avec OpenStreetMap. L'objectif de la piste est de présenter la recherche et l'innovation des investigations scientifiques sur OpenStreetMap et au-delà comme une voie pour explorer les sciences de l'information géographiques sur la base de la plateforme OSM en Afrique. **L’Empreinte ou la piste académique** du SotM Africa vise également à mettre en réseau les membres de la communauté OpenStreetMap et la communauté universitaire en Afrique par le biais d'une collaboration ouverte et d'un échange d'idées pour piloter le projet OpenStreetMap et ouvrir les données cartographiques pour la recherche et la géoinformation en Afrique. Nous attendons des contributions empiriques, méthodologiques, conceptuelles ou basées sur l'examen de la littérature traitant de tout aspect scientifique lié à OpenStreetMap, en particulier, mais sans s'y limiter, les éléments suivants :

- Évaluation de la qualité extrinsèque ou intrinsèque des données OpenStreetMap
- Analyse des modèles de contribution dans OpenStreetMap
- Génération de nouveaux ensembles de données scientifiquement utiles à partir d'OpenStreetMap
- Évaluations des procédures d'importation de données et de leurs impacts sur les données et la communauté
- Intégration entre OpenStreetMap et d'autres sources de données (autorisées, générées par l'utilisateur ou autrement précieuses pour OpenStreetMap)
- Analyse/comparaison des logiciels disponibles à des fins scientifiques liés à OpenStreetMap
- Nouvelles approches pour faciliter ou améliorer la collecte de données et/ou la qualité des données dans OpenStreetMap (par exemple, grâce à des approches de gamification ou de science citoyenne)
- Intelligence artificielle/apprentissage automatique à partir de et avec OpenStreetMap (par exemple, cartographie assistée par l'IA)
- Problèmes de recherche ouverts dans OpenStreetMap et défis pour la communauté scientifique
- Aspects culturels, politiques et organisationnels de la production de données et des pratiques d'utilisation dans OpenStreetMap
- Études utilisant les données OpenStreetMap dans les domaines scientifiques
- Examens de tout aspect scientifique lié à OpenStreetMap
- Éducation OpenStreetMap - Enseignants dans les écoles
- Cyber ​​Cartographie et OpenStreetMap
- Et beaucoup plus

Dans un effort pour déclencher une interaction et une collaboration durables entre les universitaires et les communautés OpenStreetMap plus générales, les auteurs sont invités à souligner en particulier les implications pratiques ou les impacts de leurs recherches sur la communauté OpenStreetMap dans son ensemble.

#### Directives de soumission

Les auteurs sont invités à soumettre des résumés détaillés en utilisant le système de soumission State of the Map 2023 Pretalx. La date limite de soumission est le 30 juin 2023.
Les résumés doivent avoir entre 800 et 1200 mots. Ces limites seront strictement appliquées pour un processus d'examen juste et équilibré.
Les résumés doivent être scientifiquement rigoureux et le contenu doit être logiquement structuré comme suit (sans qu'il soit nécessaire d'inclure des sous-sections) : introduction/contexte, où le problème abordé est introduit ; objectif ou objet principal de l'étude ; brève description de la méthodologie et des résultats obtenus ; discussion finale soulignant la contribution scientifique de l'étude et ses avantages/implications pratiques.
Lors de l'évaluation des propositions, le comité scientifique sera attentif à la reproductibilité de la recherche (le cas échéant). La reproductibilité est assurée lorsque la recherche rend tous les artefacts (données d'entrée, étapes de calcul, méthodes et code) librement disponibles pour obtenir des résultats cohérents. Lorsqu'il est disponible, le code doit être publié sous une licence open source.
Les résumés doivent être soumis en ligne en format texte brut (pas d'images ni de figures).
Les résumés seront évalués par le comité scientifique. Les auteurs des résumés sélectionnés seront invités à faire une présentation orale lors des sessions Empreinte Académique de la conférence ou à présenter un poster (au cas où une session poster serait organisée). Les résumés sélectionnés seront publiés sous forme de collection, chacun avec un identifiant d'objet numérique (DOI) distinct dans Zenodo, un référentiel en ligne en libre accès.
Le comité scientifique pourra chercher à diffuser davantage les contributions à cette conférence en étudiant l'organisation d'un numéro spécial dans une revue scientifique pertinente en libre accès. Dans ce cas, les auteurs des résumés sélectionnés seront invités à soumettre un article complet pour ce numéro spécial. Les soumissions retenues peuvent bénéficier d'une exonération partielle ou totale des frais de publication.

#### Comment nous contacter

Vous pouvez nous adresser toutes vos communications à l'adresse [academic@stateofthemap.africa](mailto:academic@stateofthemap.africa)

#### Code de conduite

State of the Map Africa (SotM Africa) s'engage à offrir une expérience positive de la conférence à tous les participants, quels que soient leur sexe, leur identité et leur expression sexuelles, leur orientation sexuelle, leur handicap, leur apparence physique, leur race, leur âge, leur religion ou leur origine nationale ou ethnique. Nous encourageons les interactions respectueuses et attentionnées entre les participants et ne tolérons pas le harcèlement des participants à la conférence sous quelque forme que ce soit. Nous considérons que l'utilisation d'un langage offensant, la promotion du sexe et les images érotiques sont inappropriés pour toute conférence et tout réseautage. Les participants à la conférence qui enfreignent ces normes peuvent être sanctionnés ou expulsés de la conférence (sans remboursement) à la discrétion des organisateurs de la conférence. Notre politique anti-harcèlement peut être consultée [ici](/fr/code-of-conduct.md).

#### Critères de révision

Vous trouverez ci-dessous les directives et les processus qui seront utilisés pour l'examen de votre proposition.

Le rôle des évaluateurs est de garantir la qualité du contenu présenté au State of the Map Africa.

#### Conflit d'intérêt

En cas de conflit d'intérêt, l'évaluateur doit immédiatement en informer le comité de programme et s'engager à se retirer de l'évaluation. Un remplaçant sera également désigné rapidement.

#### Critères d'évaluation

Nous évaluerons la proposition soumise sur la base des critères suivants.

- Le sujet et le contenu de la proposition sont innovants et opportuns.
- La proposition s'aligne clairement au moins sur le thème et les objectifs de la conférence.
- Les présentations proposées présentent des approches, des concepts et des stratégies qui peuvent être mis en œuvre dans toutes les disciplines.
- La proposition est concise, claire, bien organisée et traite des questions et développements actuels dans la communauté de la cartographie ouverte.
- La proposition présente des résultats d'apprentissage clairs pour les participants.

#### Calendrier

- **1 avril 2023** : Appel à propositions
- **24 juin 2023** : Date limite pour les soumissions générales
- **24 juin 2023** : Date limite de soumission des exposés académiques
- **30 novembre** : 2 décembre 2023 : État de la carte 2023

#### Soumission

Veuillez soumettre votre proposition de présentation à notre [formulaire](https://pretalx.com/sotm-africa-2023/cfp) de soumission.

#### Scientific Committee

1. Victor N. Sunday - Geography and Environmental Management, University of Port Harcourt. (Co-chair).

2. Prof. Joseph Oloukoi – Dean of Academics, African Regional Institute for Geospatial Information Science and Technology (Co-chair).

3. Prof. Joel Umeuduji – Geography and Environmental Management, University of Port Harcourt, Nigeria.

4. Dr. Raphael Ike Ndukwu – Geoinformatics and Surveying Department, University of Nigeria, Enugu Campus.

5. Dr. Uchechi S. Anaduaka - Economics Department, University of Nigeria, Nsukka.

6. Dr. Henry Ndaimani – Geography Department, University of Zimbabwe.

7. Dr. C. Anthony Onyekwelu- Geography Department, University of Nigeria Nsukka.
