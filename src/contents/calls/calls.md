# General Tracks

From the 30th of November to the 2nd of December 2023, the OSM Africa community will be coming together for yet another exciting conference: **State of the Map Africa, 2023.** The conference aims to bring together OpenStreetMap enthusiasts from Africa and beyond.

As open-source and collaborative platforms continue to evolve, this year’s conference aims to highlight how open mapping has been and continues to play a powerful role in local development in Africa. Open mapping continues to empower communities to better understand and address their own needs and challenges, from improving infrastructure and services to responding to other emergencies including natural disasters, thereby driving a positive change.

The conference will provide an opportunity for mappers to share and exchange ideas, knowledge, and experience; expand their network, and generate ideas to grow OSM within the continent. We are inviting members of the OpenStreetMap community and other OSM data users to share with us about your mapping projects and ideas, OSM experiences at an individual, community or institutional level. From beginner mappers, OSM data users, to community leaders, we would like to hear from you!
With this year’s theme being **Open mapping as a support tool for local development in Africa**, we are keen to learn about:

- Collaboration between stakeholders to promote open mapping tools for local development.
- How open data has helped governments and communities to support interventions, e.g health-related projects, humanitarian projects, etc
- How is Mapping helping Africa to foster development?
- Mapping projects that highlights how open mapping can drive positive change for local communities
- The future of mapping: how OpenStreetMap is changing the game
- Policy support for open mapping tools to promote data sharing and collaboration.
- Engaging with local communities to ensure open mapping tools reflect their needs and priorities.
- Mapping collaborations across different entities, communities, and countries.

#### Tracks

We are looking for submissions in these categories:

- **Cartography and data visualization**

  By creating maps and visualization that are accessible and easy to understand, communities can use data to identify patterns, trends and improve their quality of lifeThis track pertains to everything on map-making and data visualization. We welcome all submissions that demonstrate the use of OpenStreetMap data and platforms, tools, and software like QGIS, GRASSGIS, Blender, etc., to produce and share compelling maps and visualizations used to identify areas of needs, plan service delivery, and monitor progress overtime.

- **Mapping**

There are several ways that one can contribute to OSM and edit the map! In this track, we would like to hear how you are using existing and new tools, mobile applications, and platforms for data collection, editing, and validation. This also includes testing experience, pros and cons of each, etc.

- **Data analysis**

This track is about research, projects, and scientific work done to assess OpenStreetMap data quality, contribution patterns, and usage.

- **Community**

This track is focused on presentations about open communities and networks in open mapping and the general geospatial space. From local and regional OSM communities, YouthMappers chapters, OSGeo chapters to other group networks, we would like to hear about your community activities, sustainability plans, and initiations to improve community growth, diversity, and inclusion.

- **Innovation track**
  Have you built web-based solutions using OSM? We would like to hear your process for designing and developing web applications, portals, and story maps using the OpenStreetMap API, different libraries, and third-party platforms. We expect to see even more innovative use of open mapping and we would love to see people with idea on what the future looks like in terms of innovation.
  Note that a separate academic track will also be organized this time, stay tuned if this would better fit your subject.

- **Scientific computing**

This track is dedicated to individuals and/or organizations who would like to share their use cases on mapping with artificial intelligence, machine learning, reality technology (AR/VR/MR), etc.

#### Submission Types

- **Talks** This session is primarily dedicated to sharing your work with the OSM community, whether it is related to mapping, research, application development from other/specific fields.
  - **Talks**: Are usually 20 minutes long with additional 5 minutes for Q&A.
  - **Lighting Talks**: Are quick 5 minutes talks with additional 2 minutes for Q&A.
- **Panel** This session is dedicated to hosting a group of three or four to discuss and highlight a particular topic of interest. The panel is 45 minutes long, with additional 15 minutes for Q&A.
- **Workshop** Workshops will be an hour to two hours long. The presenter will facilitate hand-on sessions that demonstrate technical concepts to allow attendees to follow the demonstration easily. Workshops will take place before and during the conference. This will include simple and introductory sessions on getting started with OSM and more technical ones showing mapping-specific tutorials, geospatial package development, image classification, AI/ML applications in mapping.
- **Panel/Roundtable discussion**
  Panels and roundtables will be an hour long.These discussions will provide a valuable forum for a diverse range of stakeholders to share best practices, explore the opportunities and challenges in open mapping. Participants might include community leaders, local organizations, development practitioners, government officials and academics, among others.

The discussion could explore different themes, such as :

- Importance of building capacity for open mapping in Africa. This could include training and education programs, as well as efforts to promote more inclusive and participatory approaches to mapping.

- How to develop more robust and sustainable funding models for open mapping initiatives. While many projects have been successful, securing long-term funding and support can be a challenge. This group can explore strategies for building sustainable funding models that can support open mapping efforts on a long term.

- Addressing gender-based barriers to participation in open mapping. Women face challenges related to access to technology and cultural norms that limit their ability to participate in mapping activities. This group can explore strategies for addressing these barriers and promoting inclusive participation, building trust and relationships with women’s groups through outreach and engagement efforts.

- Ensuring that women’s needs and perspectives are reflected in mapping data. Mapping data can be more inclusive and reflective of women’s lives if strategies can be developed to ensure that. This group can explore how understanding women’s needs and getting their perspectives can help improve mapping and also strategies for engaging women in mapping.

**For the panel/roundtable discussion, we want submissions to reflect relevant expertise in selected themes and also unique perspectives that you will bring to the discussion. Submissions should be focused on just one [1] theme from those listed above**

Other sessions types include:

- Bird of a feather sessions(BoFs)
- Poster presentations
- Drone mapping sessions

#### Submission Details

- Recording and Material
  - This is dependent on the decision on whether we'll have a physical or online conference. But ideally, the SotM Africa team plans to record all sessions during the event.
- Conference proceeding
  - TBD
- Registration of speakers:
  - To participate at the 2023 State of the Map Africa conference, all speakers and attendees will be required to register for the conference. More details will be announced soon.

##### How to contact us for general track

You can direct all communications to us through [program@stateofthemap.africa](mailto:program@stateofthemap.africa)

# Academic Track

This year, the State of the Map Africa conference 2023, will feature the first edition of the Academic Track - a full day of sessions dedicated to academic research about, and with, OpenStreetMap. The goal of the Track is to showcase the research and innovation of scientific investigations into OpenStreetMap and beyond as a pathway to exploring OSM GISciences in Africa. The SotM Africa Academic Track also aims at networking members of the OpenStreetMap community and the academic community in Africa through an open collaboration and exchange of ideas to drive OpenStreetMap project and open map data for research and geoinformation in Africa. We expect empirical, methodological, conceptual, or literature-review-based contributions addressing any scientific aspect related to OpenStreetMap, in particular, but not limited to, the following:

- Extrinsic or intrinsic quality assessment of OpenStreetMap data
- Analysis of contribution patterns in OpenStreetMap
- Generation of new and scientifically valuable datasets from OpenStreetMap
- Assessments of data import procedures and their impacts on data and community
- Integration between OpenStreetMap and other data sources (authoritative, user-generated, or otherwise valuable to OpenStreetMap)
- Analysis/comparison of available software for scientific purposes related to OpenStreetMap
- Novel approaches to facilitate or improve data collection and/or data quality in OpenStreetMap (e.g. through gamification or citizen science approaches)
- Artificial Intelligence / Machine Learning from, and with OpenStreetMap (e.g. AI-assisted mapping)
- Open research problems in OpenStreetMap and challenges for the scientific community
- Cultural, political, and organisational aspects of data production and usage practices in OpenStreetMap
- Studies using OpenStreetMap data in scientific domains
- Reviews of any scientific aspect connected to OpenStreetMap
- OpenStreetMap education-Teachers in schools
- Cyber Cartography and OpenStreetMap
- And Lots more

In an effort to trigger a sustainable interaction and collaboration between the academic and the more general OpenStreetMap communities, authors are invited to particularly highlight the practical implications or impacts of their research on the OpenStreetMap community at large.

**Submission guidelines for academic track**

Authors are invited to submit extended abstracts using the State of the Map 2023 Pretalx submission system. Deadline for submission is 24 June 2023.
Abstracts should be between 800 and 1200 words. These limits will be strictly enforced for a fair and balanced review process.
Abstracts must be scientifically rigorous and the content should be logically structured as follows (without the need to include subsections): introduction/background, where the problem addressed is introduced; main aim or purpose of the study; brief description of the methodology and findings achieved; final discussion highlighting the scientific contribution of the study and its practical benefits/implications.

In the evaluation of proposals, the scientific committee will pay attention to the reproducibility of the research (where this is applicable). Reproducibility is ensured when the research makes all artefacts (input data, computational steps, methods and code) openly available to obtain consistent results. When available, the code shall be released under an open-source licence.

Abstracts are to be submitted online in plain-text format (no images or figures).
Abstracts will be evaluated by the scientific committee. Authors of selected abstracts will be invited to deliver an oral presentation during the Academic Track sessions at the conference or to present a poster (in case a poster session will be organised). Selected abstracts will be published as a collection, each with a distinct Digital Object Identifier (DOI) in Zenodo, an open access online repository.
The scientific committee may seek to further disseminate the contributions to this conference by investigating the organisation of a special issue in a relevant, open access, scientific journal. In such a case, authors of the selected abstracts will be invited to submit a full paper to this special issue. Successful submissions may benefit from partial or full waiver of publication fees.

#### How to contact us for academic track

You can direct all communications to us through [academic@stateofthemap.africa](mailto:academic@stateofthemap.africa)

#### Code of Conduct

State of the Map Africa (SotM Africa) is dedicated to providing a positive conference experience for all attendees, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, race, age, religion, or national and ethnic origin. We encourage respectful and considerate interactions between attendees and do not tolerate harassment of conference participants in any form. We see the use of offensive language, sexual advancement and erotic imagery as inappropriate for any conference and networking. Conference participants violating these standards may be sanctioned or expelled from the conference (without a refund) at the discretion of the conference organizers. Our anti-harassment policy can be found [here](/code-of-conduct.md).

#### Review Guidelines

Below are the guidelines and processes that will be used in reviewing your submission.
The role of reviewers is to ensure the quality of the content presented at State of the Map Africa.

#### Conflict of interest

In the event of any conflict of interest, the reviewer will immediately signal to the programs committee and commit to withdraw from reviewing. A replacement will also be issued quickly.

#### Criteria for review

We shall evaluate the submitted proposal based on the following criteria.

- The proposal topic and content are innovative and timely.
- The proposal clearly aligns with at least the theme and goals of the conference.
- The proposed presentations have approaches, concepts, and strategies that can be implemented across disciplines.
- The proposal is concise, clear, well organized, and addresses current issues and development in the open mapping community.
- The proposal has clear learning outcomes for attendees.

#### Timeline and Deadlines

- **1st May 2023**: Call for proposals is open
- **24th June 2023**: Deadline general submissions
- **24th June 2023**: Deadline academic talk submissions
- **30th November** - 2nd December 2023: State of the Map 2023

#### Submit your presentation

Please submit your presentation proposal to our submission [form](https://pretalx.com/sotm-africa-2023/cfp).

#### Scientific Committee

1. Victor N. Sunday - Geography and Environmental Management, University of Port Harcourt. (Co-chair).

2. Prof. Joseph Oloukoi – Dean of Academics, African Regional Institute for Geospatial Information Science and Technology (Co-chair).

3. Prof. Joel Umeuduji – Geography and Environmental Management, University of Port Harcourt, Nigeria.

4. Dr. Raphael Ike Ndukwu – Geoinformatics and Surveying Department, University of Nigeria, Enugu Campus.

5. Dr. Uchechi S. Anaduaka - Economics Department, University of Nigeria, Nsukka.

6. Dr. Henry Ndaimani – Geography Department, University of Zimbabwe.

7. Dr. C. Anthony Onyekwelu- Geography Department, University of Nigeria Nsukka.
