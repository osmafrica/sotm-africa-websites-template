#### Code de conduite

Nos activités et événements doivent favoriser une collaboration significative entre les participants, les membres de la communauté et les partenaires gouvernementaux. Nous attendons de la gouvernance interne, des participants, de la défense des intérêts, des activités et des événements qu'ils soient efficaces :

- Offrir un environnement sûr, respectueux et accueillant où chacun est libre d'exprimer pleinement ses idées et son identité sans discrimination ni harcèlement. Tous les espaces, en ligne ou physiques, sont régis par la politique anti-harcèlement de la SotM A.
- Faire preuve d'ouverture et travailler en toute objectivité.
- Collaborer dans le but de générer des idées qui responsabilisent, innovent et développent les capacités locales.
- Valoriser les autres et chérir les idées, les compétences et les contributions de chacun.
- Encouragez les participants à écouter autant qu'ils parlent, et à poser des questions respectueuses.
- Favoriser l'énergie qui construit des idées "oui et" et permettre la capacité de s'étirer.
- Construire des idées, des outils et des plateformes qui sont ouverts et gratuits pour la réutilisation. Les activités doivent privilégier l'utilisation publique, et non le gain privé.
- Impliquer activement les parties prenantes, les groupes communautaires et les experts dans tous les processus décisionnels.
- Veiller à ce que les relations et les conversations entre les participants à la conférence, les comités de SotM Africa, notre communauté d'accueil et leurs partenaires, et les membres de la communauté Openstreetmap - et entre eux - restent respectueuses, participatives et productives.

SotM Africa se réserve le droit de demander à toute personne en violation de ces politiques de ne pas participer aux événements ou au réseau d'activités de SotM Africa. Si vous avez d'autres préoccupations ou si vous souhaitez aborder un problème, veuillez contacter notre équipe à <contact@stateofthemap.africa> ou contacter l'un des responsables mentionnés à la fin de ce document.

#### Lutte contre le harcèlement

Tous les événements de SotM Africa, les lieux de travail, les stands des exposants et des vendeurs, les comités, les présentateurs et les participants sont tenus de respecter la politique anti-harcèlement suivante.

OpenStreetMap Africa et SotM Africa s'engagent à offrir une expérience de conférence sans harcèlement à tous, indépendamment du sexe, de l'identité et de l'expression sexuelles, de l'orientation sexuelle, du handicap, de l'apparence physique, de la taille, de la race, de l'âge ou de la religion. Nous ne tolérons aucune forme de harcèlement des participants à la conférence. Le langage et les images à connotation sexuelle ne sont pas appropriés dans les lieux de conférence, y compris les conférences.
Le harcèlement comprend et n'est pas limité aux commentaires verbaux qui renforcent les structures sociales de domination liées au sexe, à l'identité et à l'expression sexuelles, à l'orientation sexuelle, au handicap, à l'apparence physique, à la taille, à la race, à l'âge, à la religion ; aux images sexuelles dans les espaces publics ; à l'intimidation délibérée ; au harcèlement criminel ; au suivi ; au harcèlement de la photographie ou de l'enregistrement ; à la perturbation durable des conférences ou d'autres événements ; au contact physique inapproprié ; et à l'attention sexuelle non désirée.

#### Mise en œuvre

Les participants peuvent être invités à cesser immédiatement tout comportement de harcèlement et sont tenus de s'y conformer immédiatement.
Si un participant se livre à un comportement de harcèlement, les organisateurs de l'événement conservent le droit de prendre toutes les mesures nécessaires pour que l'événement reste un environnement accueillant pour tous les participants. Cela inclut l'avertissement du contrevenant ou l'expulsion de l'événement sans remboursement.

#### Rapports

Si quelqu'un vous fait vous sentir en danger ou importun, veuillez le signaler dès que possible aux organisateurs. Le harcèlement et les autres violations du code de conduite réduisent la valeur de notre événement pour tout le monde. Nous voulons que vous soyez heureux lors de notre événement. Des gens comme vous font de notre événement un endroit meilleur. Notre personnel et nos bénévoles seront heureux d'aider les participants à contacter la sécurité de l'hôtel/du lieu de la conférence ou les forces de l'ordre locales, de fournir des escortes ou d'aider de toute autre manière les personnes victimes de harcèlement à se sentir en sécurité pendant la durée de la conférence. Votre participation est importante pour nous.

Le document de SotM Africa sur le code de conduite et la lutte contre le harcèlement est une adaptation de :

- [Code for America’s Community Code of Conduct policy](https://github.com/codeforamerica/codeofconduct)
- [Geek Feminism Wiki](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment)
- [School of Data LA](https://schoolofdata.la/)
